import React from 'react';
import HeroSection from '../../components/HeroSection/HeroSection';
import Pricing from '../../components/Pricing/Pricing';

import { homeObjOne,homeObjThree} from './Data';


function Services() {
  return (
    <>
      <Pricing />
      <HeroSection {...homeObjOne} />
      <HeroSection {...homeObjThree} />
    </>
  );
}

export default Services;