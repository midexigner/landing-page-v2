import React from 'react';

import './App.css';
import {
  BrowserRouter as Router,
  Switch,
  Route

} from "react-router-dom";
import Home from './pages/Home/Home';
import Navbar from './components/Navbar/Navbar';
import Footer from './components/Footer/Footer';
import Services from './pages/Services/Services';
import Products from './pages/Products/Products';
import SignUp from './pages/SignUp/SignUp';

function App() {
  return (
    <div className="app">
      <Router>
        <Navbar/>
      <Switch>
        <Route exact path='/'><Home/></Route>
        <Route exact path='/services' component={Services} />
        <Route exact path='/products' component={Products} />
        <Route path='/sign-up' component={SignUp} />
     </Switch>
     <Footer />
     </Router>
    </div>
  );
}

export default App;
